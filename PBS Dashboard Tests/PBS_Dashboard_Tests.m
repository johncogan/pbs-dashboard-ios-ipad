//
//  PBS_Dashboard_Tests.m
//  PBS Dashboard Tests
//
//  Created by John Cogan on 2013/10/11.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface PBS_Dashboard_Tests : XCTestCase

@end

@implementation PBS_Dashboard_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
