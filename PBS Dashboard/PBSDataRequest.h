//
//  PBSDataRequest.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/25.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBSDataRequest : NSObject

@property NSString *targetDate;
@property NSString *calendType;

@end
