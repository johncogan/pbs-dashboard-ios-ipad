//
//  PBSViewController.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSViewController.h"
#import <Crashlytics/Crashlytics.h>

@interface PBSViewController ()

@end

@implementation PBSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated. Test 
}

@end
