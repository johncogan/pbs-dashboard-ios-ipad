//
//  PBSMonthSales.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBSDataMonthlySalesItem.h"

@interface PBSDataMonthSales : NSObject
{
    
}

@property NSMutableArray *dailySalesFigures;
@property int numDaysInMonth;

- (id)initWithSetNumberOfDays:(int)numberOfDays;

@end
