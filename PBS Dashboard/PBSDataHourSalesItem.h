//
//  PBSHourSalesItem.h
//  PBSDashboard
//
//  Created by John Cogan on 2013/09/11.
//  Copyright (c) 2013 Platinum Point of Sale Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBSDataHourSalesItem : NSObject
{
    
}

    @property (nonatomic, assign) NSUInteger hour;
    @property (nonatomic, assign) NSUInteger numberOfSales;
    @property (nonatomic, assign) float totalOfSales;

    -(id)init;
    -(id)initWithHour: (NSUInteger)hour;
@end
