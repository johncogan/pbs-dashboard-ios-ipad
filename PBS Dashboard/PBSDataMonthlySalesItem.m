//
//  PBSMonthlySalesItem.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSDataMonthlySalesItem.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSDataMonthlySalesItem

@synthesize day;
@synthesize numberOfSales;
@synthesize totalOfSales;

- (id)init
{
    if(self = [super init])
    {
        day = 0;
        numberOfSales = 0;
        totalOfSales = 0;
    }
    return self;
}

- (id)initWithDay:(NSUInteger)theDay
{
    self.day = theDay;
    return self;
}

@end
