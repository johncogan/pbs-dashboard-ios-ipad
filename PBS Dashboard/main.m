//
//  main.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PBSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PBSAppDelegate class]));
    }
}
