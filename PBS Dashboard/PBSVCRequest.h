//
//  PBSRequestViewController.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Unirest.h"
#import "PBSSales.h"
#import "PBSDataRequest.h"

@interface PBSVCRequest : UIViewController
{
    PBSSales *salesData;
}

@property (weak, nonatomic) IBOutlet UILabel *requestVCMessages;
@property (strong, nonatomic) IBOutlet NSString *userChosenDataDisplayType;
@property (weak, nonatomic) IBOutlet NSString *userChosenTargetDate;
@property (strong, nonatomic) PBSDataRequest *requestParameters;

@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (void) loadDefaultSettings;
- (void) runUnirestRequest:(NSString*)urlToSendRequestTo;
- (NSString*) buildRequestUrl:(NSString*)serverUrl withPort:(NSString*)serverPort withCalenderType:(NSString*)dataDisplayType withDateStringParameter:(NSString*)selectedTargetDate;

- (PBSSales*) deserializeJsonPacket:(NSDictionary*)jsonResponseAsDictionary withCalenderType:(NSString*)calendarType;
- (void)createActivityIndicator;

@end
