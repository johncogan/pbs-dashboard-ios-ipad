//
//  PBSYearSales.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSDataYearSales.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSDataYearSales

NSMutableArray *monthlySalesFigures;

- (id)init
{
    if(self = [super init])
    {
        _monthlySalesFigures = [[NSMutableArray alloc] init];
        
        for (NSInteger i = 0; i < 12; ++i)
        {
            PBSDataYearlySalesItem *tmpFigures = [[PBSDataYearlySalesItem alloc] init];
            [tmpFigures setMonth:i];
            [tmpFigures setNumberOfSales:0];
            [tmpFigures setTotalOfSales:0];
            
            [_monthlySalesFigures addObject:tmpFigures];
        }
        
    }
    return self;
}

@end
