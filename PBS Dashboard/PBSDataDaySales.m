//
//  PBSDaySales.m
//  PBSDashboard
//
//  Created by John Cogan on 2013/09/11.
//  Copyright (c) 2013 Platinum Point of Sale Business Solutions. All rights reserved.
//

#import "PBSDataDaySales.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSDataDaySales

// @synthesize hourlySalesFigures;

NSMutableArray *hourlySalesFigures;

- (id)init
{
    if(self = [super init])
    {
        _hourlySalesFigures = [[NSMutableArray alloc] init];
        
        for (NSInteger i = 0; i < 24; ++i)
        {
            PBSDataHourSalesItem *tmpFigures = [[PBSDataHourSalesItem alloc] init];
            [tmpFigures setHour:i];
            [tmpFigures setNumberOfSales:0];
            [tmpFigures setTotalOfSales:0];
            
            [_hourlySalesFigures addObject:tmpFigures];
        }
        
    }
    return self;
}
@end
