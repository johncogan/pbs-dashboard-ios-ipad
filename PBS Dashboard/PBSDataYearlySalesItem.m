//
//  YearlySalesFigures.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSDataYearlySalesItem.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSDataYearlySalesItem
{
    
}

@synthesize month;
@synthesize numberOfSales;
@synthesize totalOfSales;

- (id)init
{
    if(self = [super init])
    {
        month = 0;
        numberOfSales = 0;
        totalOfSales = 0;
    }
    return self;
}

- (id)initWithMonth:(NSUInteger)theMonth
{
    self.month = theMonth;
    self.numberOfSales = 0;
    self.totalOfSales = 0;
    
    return self;
}
@end
