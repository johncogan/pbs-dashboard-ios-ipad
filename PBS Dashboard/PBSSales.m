//
//  PBSSales.m
//  PBSDashboard
//
//  Created by John Cogan on 2013/09/11.
//  Copyright (c) 2013 Platinum Point of Sale Business Solutions. All rights reserved.
//

#import "PBSSales.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSSales

@synthesize error, targetDate, previousDate, nextDate;
@synthesize numberOfCashSales, numberOfCardSales, numberOfChequeSales, numberOfLoyaltySales;
@synthesize numberOfChargeSales, totalCash, totalCard, totalLoyalty, totalCharge, totalCheque;
@synthesize totalGrand;

@synthesize calViewType;

@synthesize dataDay;
@synthesize dataMonth;
@synthesize dataYear;
@synthesize navTitle;

-(id)init
{
    if(self = [super init])
    {
        self.dataDay = [[PBSDataDaySales alloc] init];
        self.dataMonth = [[PBSDataMonthSales alloc] init];
        self.dataYear = [[PBSDataYearSales alloc] init];
    }
    
    return self;
}

/*
    Resets calViewType to nil
 */
-(void)reset
{
    self.calViewType = nil;
}

- (NSString*)getNavControlTitle
{
    NSMutableString *titleForPages = [[NSMutableString alloc] init];
    
    if([self.calViewType  isEqual: @"Day"])
    {
        [titleForPages setString:@"Sales for "];
    }
    
    if([self.calViewType  isEqual: @"Month"])
    {
        [titleForPages setString:@"Sales for "];
    }
    
    if([self.calViewType  isEqual: @"Year"])
    {
        [titleForPages setString:@"Sales for "];
    }
    
    if(targetDate != nil){
    [titleForPages appendString:[self getDatePartForTitle:targetDate]];
    
        self.navTitle = titleForPages;
    }else{
        self.navTitle = @"";
        
    }
    return navTitle;
}

-(NSString*) getDatePartForTitle:(NSString*)withStringDate
{
    NSString* theDateAsString = withStringDate;
    NSString* theDatePartReturning;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [dateFormat dateFromString:theDateAsString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    if([self.calViewType  isEqual: @"Day"])
    {
        [formatter setDateFormat:@"dd LLL yyyy"];
    }
    
    if([self.calViewType  isEqual: @"Month"])
    {
        [formatter setDateFormat:@"LLL yyyy"];
    }
    
    if([self.calViewType  isEqual: @"Year"])
    {
        [formatter setDateFormat:@"yyyy"];
    }
    
    theDatePartReturning = [formatter stringFromDate:date];
    
    return theDatePartReturning;
}

/*
 Determine what the highet value is of all the values to be charted
 */
-(float)chartYAxisHighestValue
{
    float highestValue = 0;
    
    if([self.calViewType  isEqual: @"Day"])
    {
        for (PBSDataHourSalesItem *PBSHourItem in [[self dataDay] hourlySalesFigures]) {
            if([[PBSHourItem valueForKey:@"totalOfSales"] floatValue] > highestValue)
                highestValue = [[PBSHourItem valueForKey:@"totalOfSales"] floatValue];
        }
    }
    
    if([self.calViewType  isEqual: @"Month"])
    {
        for (PBSDataMonthlySalesItem *PBSDayItem in [[self dataMonth] dailySalesFigures]) {
            if([[PBSDayItem valueForKey:@"totalOfSales"] floatValue] > highestValue)
                highestValue = [[PBSDayItem valueForKey:@"totalOfSales"] floatValue];
        }
    }
    
    if([self.calViewType  isEqual: @"Year"])
    {
        for (PBSDataYearlySalesItem *PBSMonthItem in [[self dataYear] monthlySalesFigures]) {
            if([[PBSMonthItem valueForKey:@"totalOfSales"] floatValue] > highestValue)
                highestValue = [[PBSMonthItem valueForKey:@"totalOfSales"] floatValue];
        }
    }
    
    return highestValue;
}

/*
 Determine what the Y Axis maximum value is to be. Should be the next value that is higher than
 the value found via the method 'chartYAxisHighestValue'
*/
-(float)chartCalculateYMaxRange
{
    float highestActualValue = [self chartYAxisHighestValue];
    float valueForYRange = 0;
    NSArray *maxRanges = [NSArray arrayWithObjects:@"1000",@"5000",@"10000",@"15000",@"20000",
                          @"25000",@"30000",@"35000",@"40000",@"45000",
                          @"50000",@"60000",@"70000",@"80000",@"90000",
                          @"100000",@"150000",@"200000",@"250000",
                          @"300000",@"350000",@"400000",@"450000",
                          @"500000",@"600000",@"700000",@"800000",
                          @"900000",@"1000000",@"1500000",@"2000000",@"2500000",
                          @"3000000",@"3500000",@"4000000",@"4500000",
                          @"5000000",@"6000000",@"7000000",@"8000000",
                          @"9000000",@"10000000",
                          nil];
    
    for(int i = 0; i < [maxRanges count]; i++){
        if(highestActualValue < [maxRanges[i] floatValue]){
            valueForYRange = [maxRanges[i] floatValue];
            break;
        }
    }
    
    return valueForYRange;
}

/*
    How many records are there to be charted
*/
-(int)numberOfRecords
{
    if([self.calViewType  isEqual: @"Day"])
        return [[[self dataDay] hourlySalesFigures] count];
    
    if([self.calViewType  isEqual: @"Month"])
        return [[[self dataMonth] dailySalesFigures] count];
    
    if([self.calViewType  isEqual: @"Year"])
        return [[[self dataYear] monthlySalesFigures] count];
    
    return 0;
}

-(NSString *) chartGetLineOneChartTitle
{
    if([self.calViewType  isEqual: @"Day"])
        return @"Sales - Day";
    
    if([self.calViewType  isEqual: @"Month"])
        return @"Sales - Month";
    
    if([self.calViewType  isEqual: @"Year"])
        return @"Sales - Year";
    
    return nil;
}

-(NSString *) chartGetLineTwoChartTitle
{
    //NSDate *dateFromDatePicker = [NSDate]
    // targetDate
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [dateFormatter dateFromString:targetDate];
    
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    return formattedDate;
}

-(NSMutableArray *)chartGetTickLocations
{
    NSMutableArray *tickLocals = [NSMutableArray arrayWithCapacity:[self numberOfRecords]];
    
    for(int i = 0; i < [self numberOfRecords]; i++)
    {
        [tickLocals addObject:[NSDecimalNumber numberWithInt:i]];
    }
    
    return tickLocals;
}

-(NSMutableArray *)chartGetXAxisLabels
{
    NSMutableArray *xAxisLabels = [NSMutableArray arrayWithCapacity:[self numberOfRecords]];
    
    if([[self isDayMonthYearData]  isEqual: @"Day"])
    {
        for (PBSDataHourSalesItem *PBSHourItem in [[self dataDay] hourlySalesFigures]) {
            [xAxisLabels addObject:[[PBSHourItem valueForKey:@"hour"] stringValue]];
        }
    }
    
    if([[self isDayMonthYearData]  isEqual: @"Month"])
    {
        for (PBSDataMonthlySalesItem *PBSDayItem in [[self dataMonth] dailySalesFigures]) {
            [xAxisLabels addObject:[[PBSDayItem valueForKey:@"day"] stringValue]];
        }
    }
    
    if([[self isDayMonthYearData]  isEqual: @"Year"])
    {
        for (PBSDataYearlySalesItem *PBSMonthItem in [[self dataYear] monthlySalesFigures]) {
            [xAxisLabels addObject:[[PBSMonthItem valueForKey:@"month"] stringValue]];
        }
    }
    
    return xAxisLabels;
}

-(NSString *)isDayMonthYearData
{
    if([self.calViewType  isEqual: @"Day"])
        return @"Day";
    
    if([self.calViewType  isEqual: @"Month"])
        return @"Month";
    
    if([self.calViewType  isEqual: @"Year"])
        return @"Year";
    
    return nil;
}

-(NSMutableString*)convertQuantityItemForLabel:(NSNumber*)numberToConvert
{
    NSMutableString *itemFormattedForNSLabelDisplay = [[NSMutableString alloc] init];
    
    if(numberToConvert == nil)
    {
        [itemFormattedForNSLabelDisplay setString:@""];
    }else{
        [itemFormattedForNSLabelDisplay setString:[numberToConvert stringValue]];
    }
    
    
    //[itemFormattedForNSLabelDisplay appendString:@" sales"];
    
    return itemFormattedForNSLabelDisplay;
}

-(NSMutableString*)convertTotalForLabel:(NSNumber*)numberToConvert
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    NSString *convertNumber = [formatter stringForObjectValue:numberToConvert];
    
    NSMutableString *totalForNSLabelDisplay = [[NSMutableString alloc] init];
    
    [totalForNSLabelDisplay setString:@"R "];
    [totalForNSLabelDisplay appendString:convertNumber];
    
    return totalForNSLabelDisplay;
}

-(NSString*) getError{
    return [self error];
}

-(NSMutableString*) getTotalGrandFormattedForLabel{
    return [self convertTotalForLabel:totalGrand];
}

-(NSMutableString*) getNumberOfChargeSalesFormattedForLabel{
    return [self convertQuantityItemForLabel:numberOfChargeSales];
}

-(NSMutableString*) getTotalChargeFormattedForLabel{
    return [self convertTotalForLabel:totalCharge];
}

-(NSMutableString*) getNumberOfLoyaltySalesFormattedForLabel{
    return [self convertQuantityItemForLabel:numberOfLoyaltySales];
}

-(NSMutableString*) getTotalLoyaltyFormattedForLabel{
    return [self convertTotalForLabel:totalLoyalty];
}

-(NSMutableString*) getNumberOfCardSalesFormattedForLabel{
    return [self convertQuantityItemForLabel:numberOfCardSales];
}

-(NSMutableString*) getTotalCardFormattedForLabel{
    return [self convertTotalForLabel:totalCard];
}

-(NSMutableString*) getNumberOfChequeSalesFormattedForLabel{
    return [self convertQuantityItemForLabel:numberOfChequeSales];
}

-(NSMutableString*) getTotalChequeFormattedForLabel{
    return [self convertTotalForLabel:totalCheque];
}

-(NSMutableString*) getNumberOfCashSalesFormattedForLabel{
    return [self convertQuantityItemForLabel:numberOfCashSales];
}

-(NSMutableString*) getTotalCashFormattedForLabel{
    return [self convertTotalForLabel:totalCash];
}

@end
