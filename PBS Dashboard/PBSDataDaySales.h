//
//  PBSDaySales.h
//  PBSDashboard
//
//  Created by John Cogan on 2013/09/11.
//  Copyright (c) 2013 Platinum Point of Sale Business Solutions. All rights reserved.
//

#import "PBSDataHourSalesItem.h"

@interface PBSDataDaySales : NSObject
{
    
}
    @property (strong, nonatomic) NSMutableArray *hourlySalesFigures;

    -(id)init;
@end
