//
//  YearlySalesFigures.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBSDataYearlySalesItem : NSObject
{
    
}

@property (nonatomic, assign) NSUInteger month;
@property (nonatomic, assign) NSUInteger numberOfSales;
@property (nonatomic, assign) float totalOfSales;

-(id)init;
-(id)initWithMonth: (NSUInteger)month;
@end
