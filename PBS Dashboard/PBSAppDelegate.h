//
//  PBSAppDelegate.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBSVCChartDisplay.h"

@interface PBSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PBSVCChartDisplay *pbsVCChartDisplay;

@end
