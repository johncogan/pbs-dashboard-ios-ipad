//
//  PBSMonthSales.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSDataMonthSales.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSDataMonthSales

//@synthesize monthlySalesFigures;
@synthesize numDaysInMonth;

NSMutableArray *monthlySalesFigures;

int numDaysInMonth;


- (id)initWithSetNumberOfDays:(int)numberOfDays
{
    self = [self init];
    
    _dailySalesFigures = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < numberOfDays; i++)
    {
        PBSDataMonthlySalesItem *monthSalesItem = [[PBSDataMonthlySalesItem alloc] initWithDay:i];
        [monthSalesItem setTotalOfSales:0];
        [monthSalesItem setNumberOfSales:0];
        
        self.dailySalesFigures[i] = monthSalesItem;
    }
    
    return self;
}

@end
