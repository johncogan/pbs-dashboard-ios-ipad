//
//  PBSDataDisplayViewController.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBSSales.h"


@interface PBSVCDataDisplay : UIViewController

@property (strong, nonatomic) PBSSales *salesData;

@property (weak, nonatomic) IBOutlet UILabel *cashItemQuantity;
@property (weak, nonatomic) IBOutlet UILabel *cashTotal;

@property (weak, nonatomic) IBOutlet UILabel *cardItemQuantity;
@property (weak, nonatomic) IBOutlet UILabel *cardTotal;
@property (weak, nonatomic) IBOutlet UILabel *chequeItemQuantity;
@property (weak, nonatomic) IBOutlet UILabel *chequeTotal;
@property (weak, nonatomic) IBOutlet UILabel *loyaltyItemQuantity;
@property (weak, nonatomic) IBOutlet UILabel *loyaltyTotal;
@property (weak, nonatomic) IBOutlet UILabel *chargeItemQuantity;
@property (weak, nonatomic) IBOutlet UILabel *chargeTotal;
@property (weak, nonatomic) IBOutlet UILabel *grandTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnGoChart;

- (IBAction)unWindToHome:(id)sender;

- (IBAction)pushToChartVC;
-(void)resizeScrollView;
-(void)customiseControls;


@property (strong, nonatomic) IBOutlet UIView *childView;

@property (weak, nonatomic) IBOutlet UIScrollView *detailDataScrollView;



@end
