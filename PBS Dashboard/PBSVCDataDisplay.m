//
//  PBSDataDisplayViewController.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSVCDataDisplay.h"
#import "PBSVCChartDisplay.h"
#import "PBSVCWelcome.h"
#import <Crashlytics/Crashlytics.h>

@interface PBSVCDataDisplay ()

@end

@implementation PBSVCDataDisplay

@synthesize salesData;
@synthesize detailDataScrollView;

bool noDataError = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)customiseControls{
    UIColor *ios7DefaultBlue = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255/255 alpha:1.0f].CGColor;
    
    [[_btnGoChart layer] setBorderWidth:1.0f];
    _btnGoChart.layer.cornerRadius = 5.0f;
    _btnGoChart.clipsToBounds = YES;
    [[_btnGoChart layer] setBorderColor:(__bridge CGColorRef)(ios7DefaultBlue)];
    [[_btnGoChart layer] setBackgroundColor:(__bridge CGColorRef)(ios7DefaultBlue)];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self customiseControls];
}

- (IBAction)pushToChartVC
{    
    PBSVCChartDisplay *chartViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChartVCID"];
    [chartViewController setSalesData:salesData];
    [self.navigationController pushViewController: chartViewController animated:YES];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self resizeScrollView];
}

-(void)resizeScrollView
{
    [detailDataScrollView setScrollEnabled:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    /*
     NSString *errorMessage = [[NSString alloc] init];
    errorMessage = [salesData getError];
    
    if([errorMessage isEqual:@"NODATA"] || [errorMessage isEqual:@"InvalidPassphrase"]){
        noDataError = YES;
        [self performSegueWithIdentifier:@"unWind2" sender:self];
    }
    */
    
    NSString *errorMessage = [[NSString alloc] init];
    errorMessage = [salesData getError];
    
    if([errorMessage isEqual:@"NODATA"] || [errorMessage isEqual:@"InvalidPassphrase"]){
        noDataError = YES;
        [self performSegueWithIdentifier:@"unWind2" sender:self];
        //[self.navigationController popToRootViewControllerAnimated:NO];
    }else{
        
        // NSLog(@"%@", self.navigationController.viewControllers ); // At index 2
        UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:2];
        navCon.navigationItem.title = [salesData getNavControlTitle];
        //self.navigationController.title = [salesData getNavControlTitle];
        
        [[self cashItemQuantity] setText:[salesData getNumberOfCashSalesFormattedForLabel]];
        [[self cashTotal] setText:[salesData getTotalCashFormattedForLabel]];
        
        [[self chequeItemQuantity] setText:[salesData getNumberOfChequeSalesFormattedForLabel]];
        [[self chequeTotal] setText:[salesData getTotalChequeFormattedForLabel]];
        
        [[self chargeItemQuantity] setText:[salesData getNumberOfChargeSalesFormattedForLabel]];
        [[self chargeTotal] setText:[salesData getTotalChargeFormattedForLabel]];
        
        [[self loyaltyItemQuantity] setText:[salesData getNumberOfLoyaltySalesFormattedForLabel]];
        [[self loyaltyTotal] setText:[salesData getTotalLoyaltyFormattedForLabel]];
        
        [[self cardItemQuantity] setText:[salesData getNumberOfCardSalesFormattedForLabel]];
        [[self cardTotal] setText:[salesData getTotalCardFormattedForLabel]];
        
        [[self grandTotal] setText:[salesData getTotalGrandFormattedForLabel]];
        
        // We want to "unwind" backwards 2 places to skip the Request VC and get directly to the RootController
        // but need to do this on the UINavigation items Back button which normally takes you back one place.
        // So we have to over ride the back button by creating a new one and giving it a method call to the
        // unwind segue
    }
    
    UIBarButtonItem *buttonHome = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(unWindToHome:)];
    
    self.navigationItem.leftBarButtonItem = buttonHome;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    
    if([segue.identifier isEqualToString:@"goToChart"]){
        // If the segue been called is "showDataChart", pass it the daySalesFigures data object
        PBSVCDataDisplay *dataVc = [segue destinationViewController];
        [dataVc setSalesData:salesData];
        //NSLog(@"PrepareForSegue - goToChart");
    }
    
    if([segue.identifier isEqualToString:@"showDataChart"]){
        // If the segue been called is "showDataChart", pass it the daySalesFigures data object
        
        //NSLog(@"PrepareForSegue - showDataChart");
    }
    
    if([segue.identifier isEqualToString:@"unWind2"]){
        PBSVCWelcome *welcomeVc = [segue destinationViewController];
        [welcomeVc setSalesData:salesData];
        
        //NSLog(@"PrepareForSegue - unWind2");
    }
    
    
}

// Action to send us backwards to the Home ViewController. This is an unwind segue, NOT a normal segue!
- (IBAction)unWindToHome:(id)sender {
    // [self performSegueWithIdentifier:@"unWind2" sender:self];
    //NSLog(@"IBAction - unWindToHome using popToRootViewControllerAnimated:NO");
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end
