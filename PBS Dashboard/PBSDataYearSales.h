//
//  PBSYearSales.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/01.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBSDataYearlySalesItem.h"

@interface PBSDataYearSales : NSObject
{
    
}
@property (strong, nonatomic) NSMutableArray *monthlySalesFigures;

-(id)init;

@end
