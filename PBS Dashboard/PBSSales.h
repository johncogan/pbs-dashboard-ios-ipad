//
//  PBSSales.h
//  PBSDashboard
//
//  Created by John Cogan on 2013/09/11.
//  Copyright (c) 2013 Platinum Point of Sale Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBSDataDaySales.h"
#import "PBSDataMonthSales.h"
#import "PBSDataYearSales.h"

@interface PBSSales : NSObject
{

}

    @property NSString *error, *targetDate, *previousDate, *nextDate;
    @property NSNumber *numberOfCashSales, *numberOfCardSales, *numberOfChequeSales, *numberOfLoyaltySales, *numberOfChargeSales;
    @property NSNumber *totalCash, *totalCard, *totalCheque, *totalLoyalty, *totalCharge;
    @property NSNumber *totalGrand;

    @property (nonatomic, strong) NSString *calViewType;

    @property (nonatomic, strong) PBSDataDaySales *dataDay;
    @property (nonatomic, strong) PBSDataMonthSales *dataMonth;
    @property (nonatomic, strong) PBSDataYearSales *dataYear;
    @property (nonatomic, strong) NSString *navTitle;

-(id)init;
-(void)reset;

-(int)numberOfRecords;
-(float)chartYAxisHighestValue;
-(float)chartCalculateYMaxRange;
-(NSString *) chartGetLineOneChartTitle;
-(NSString *) chartGetLineTwoChartTitle;
-(NSMutableArray *)chartGetTickLocations;
-(NSMutableArray *)chartGetXAxisLabels;
-(NSString *)isDayMonthYearData;

-(NSMutableString*)convertQuantityItemForLabel:(NSNumber*)numberToConvert;
-(NSMutableString*)convertTotalForLabel:(NSNumber*)numberToConvert;

- (NSString*)getNavControlTitle;
-(NSString*) getError;
-(NSMutableString*) getNumberOfCashSalesFormattedForLabel;
-(NSMutableString*) getTotalCashFormattedForLabel;
-(NSMutableString*) getTotalChequeFormattedForLabel;
-(NSMutableString*) getNumberOfChequeSalesFormattedForLabel;
-(NSMutableString*) getTotalCardFormattedForLabel;
-(NSMutableString*) getNumberOfCardSalesFormattedForLabel;
-(NSMutableString*) getTotalLoyaltyFormattedForLabel;
-(NSMutableString*) getNumberOfLoyaltySalesFormattedForLabel;
-(NSMutableString*) getTotalChargeFormattedForLabel;
-(NSMutableString*) getNumberOfChargeSalesFormattedForLabel;
-(NSMutableString*) getTotalGrandFormattedForLabel;

@end
