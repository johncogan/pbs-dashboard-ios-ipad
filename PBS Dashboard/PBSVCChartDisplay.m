//
//  PBSVCChartDisplay.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/02.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSVCChartDisplay.h"
#import <Crashlytics/Crashlytics.h>

@interface PBSVCChartDisplay ()

@end

@implementation PBSVCChartDisplay
{
    // Instance variables
    int numberOfRecords;
    PBSSales *salesData;
    NSMutableArray *data;
    BOOL isIPad;
}

@synthesize salesData;
@synthesize graphHostingView;
@synthesize data;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (BOOL)shouldAutorotate {
    return YES;
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}

-(void)viewWillAppear:(BOOL)animated{
    
}

-(void)viewDidAppear{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *titleString = @"Chart for ";
    NSString *salesDataNavTitleString = [salesData getNavControlTitle];
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    isIPad = YES;
    if([deviceType isEqualToString:@"iPhone"] || [deviceType isEqualToString:@"iPhone Simulator"]){
        isIPad = NO;
    }
    
    //NSLog(@"isIPad: %hhd", isIPad ); // At index 3
    UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:3];
    
    NSString *finalTitleStringForChart = [titleString stringByAppendingString:salesDataNavTitleString];
    
    navCon.navigationItem.title = finalTitleStringForChart;
    
    [self makeChart];
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return numberOfRecords;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSDecimalNumber *num = nil;
    id valueOfItem;
    NSString *valueAsStr;
    
    PBSDataHourSalesItem *tmpHourSaleItem;
    PBSDataMonthlySalesItem *tmpMonthlySaleItem;
    PBSDataYearlySalesItem *tmpYearlySaleItem;
    
    if(fieldEnum == CPTBarPlotFieldBarTip)
    {
        if([salesData.calViewType isEqualToString:@"Day"])
        {
            tmpHourSaleItem = [[[salesData dataDay] hourlySalesFigures] objectAtIndex:index];
            valueOfItem = [tmpHourSaleItem valueForKey:@"totalOfSales"];
        }
    
        if([salesData.calViewType isEqualToString:@"Month"])
        {
            tmpMonthlySaleItem = [[[salesData dataMonth] dailySalesFigures] objectAtIndex:index];
            valueOfItem = [tmpMonthlySaleItem valueForKey:@"totalOfSales"];
        }
    
        if([salesData.calViewType isEqualToString:@"Year"])
        {
            tmpYearlySaleItem = [[[salesData dataYear] monthlySalesFigures] objectAtIndex:index];
            valueOfItem = [tmpYearlySaleItem valueForKey:@"totalOfSales"];
        }
    }
    
    if ( [plot isKindOfClass:[CPTBarPlot class]] ) {
        switch ( fieldEnum ) {
            case CPTBarPlotFieldBarLocation:
                num = (NSDecimalNumber *)[NSDecimalNumber numberWithUnsignedInteger:index];
                break;
                
            case CPTBarPlotFieldBarTip:
                valueAsStr = [valueOfItem stringValue];
                num = [NSDecimalNumber decimalNumberWithString:valueAsStr];
                break;
        }
    }
    
    return num;
}

- (void) makeChart
{
    data = [[NSMutableArray alloc] init];
    numberOfRecords = [salesData numberOfRecords];
    
    float yMaxRange = [salesData chartCalculateYMaxRange];
    float xMaxRange = numberOfRecords * 1.0f;
    
    NSString *yMajorIntervalLength = [[NSNumber numberWithFloat:(yMaxRange / 10)] stringValue];

    CPTMutableTextStyle *xAxisTextStyle = [[CPTMutableTextStyle alloc] init];
    
    if(isIPad)
        xAxisTextStyle.fontSize = 12.0;
    else
        xAxisTextStyle.fontSize = 8.0;
    
    xAxisTextStyle.color = [CPTColor blackColor];
    
    CPTMutableTextStyle *yAxisTextStyle = [[CPTMutableTextStyle alloc] init];
    
    if(isIPad)
        yAxisTextStyle.fontSize = 12.0;
    else
        yAxisTextStyle.fontSize = 10.0;
        
    yAxisTextStyle.color = [CPTColor blackColor];
    
    CPTMutableLineStyle *lineStyleForGrid = [[CPTMutableLineStyle alloc] init];
    
    if(isIPad)
        lineStyleForGrid.lineWidth = 0.4;
    else
        lineStyleForGrid.lineWidth = 0.2;
    
    lineStyleForGrid.lineColor = [CPTColor blackColor];
    
    // Create barChart from theme
    
    CPTTheme *theme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    
    graphHostingView = (CPTGraphHostingView *)self.view;
    
    barChart = [[CPTXYGraph alloc] initWithFrame:graphHostingView.bounds];
    [barChart applyTheme:theme];
    
    graphHostingView.hostedGraph = barChart;
    
    // Border
    barChart.plotAreaFrame.borderLineStyle = nil;
    barChart.plotAreaFrame.cornerRadius    = 0.0f;
    barChart.plotAreaFrame.masksToBorder   = NO;
    
    // Paddings
    barChart.paddingLeft   = 0.0f;
    barChart.paddingRight  = 0.0f;
    barChart.paddingTop    = 0.0f;
    barChart.paddingBottom = 0.0f;
    
    if(isIPad) {
        barChart.plotAreaFrame.paddingLeft   = 90.0;
        barChart.plotAreaFrame.paddingTop    = 20.0;
        barChart.plotAreaFrame.paddingRight  = 20.0;
        barChart.plotAreaFrame.paddingBottom = 80.0;
    }else{
        barChart.plotAreaFrame.paddingLeft   = 60.0;
        barChart.plotAreaFrame.paddingTop    = 14.0;
        barChart.plotAreaFrame.paddingRight  = 14.0;
        barChart.plotAreaFrame.paddingBottom = 50.0;
    }
    
    // Graph title
    NSString *lineOne = [salesData chartGetLineOneChartTitle];
    NSString *lineTwo = [salesData chartGetLineTwoChartTitle];
    
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color         = [CPTColor blackColor];
    titleStyle.fontName      = @"Helvetica-Bold";
    
    if(isIPad)
        titleStyle.fontSize      = 20.0;
    else
        titleStyle.fontSize      = 14.0;
        
    titleStyle.textAlignment = CPTTextAlignmentCenter;
    
    barChart.title          = [NSString stringWithFormat:@"%@\n%@", lineOne, lineTwo];
    barChart.titleTextStyle = titleStyle;
    
    barChart.titleDisplacement        = CGPointMake(0.0f, -20.0f);
    barChart.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    
    // Add plot space for horizontal bar charts
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)barChart.defaultPlotSpace;
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0f) length:CPTDecimalFromFloat(yMaxRange)];
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-0.5f) length:CPTDecimalFromFloat(xMaxRange + 0.5f)];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)barChart.axisSet;
    
    CPTXYAxis *x          = axisSet.xAxis;
    x.axisLineStyle               = nil;
    x.majorTickLineStyle          = nil;
    x.minorTickLineStyle          = nil;
    x.majorIntervalLength         = CPTDecimalFromString(@"1");
    x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
    
    x.majorGridLineStyle = lineStyleForGrid;
    
    if([salesData.calViewType isEqualToString:@"Day"])
        x.title                       = @"Hour";
    if([salesData.calViewType isEqualToString:@"Month"])
        x.title                       = @"Day of month";
    if([salesData.calViewType isEqualToString:@"Year"])
        x.title                       = @"Month";
    
    if(isIPad)
        x.titleOffset                 = 35.0f;
    else
        x.titleOffset                 = 22.0f;
    
    // Define some custom labels for the data elements
    x.labelingPolicy = CPTAxisLabelingPolicyNone;

    NSMutableArray *customTickLocations = [salesData chartGetTickLocations];
    
    NSMutableArray *xAxisLabels = [salesData chartGetXAxisLabels];
    
    NSUInteger labelLocation     = 0;
    
    NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:[xAxisLabels count]];
    
    int count = 0;
    for ( NSNumber *tickLocation in customTickLocations ) {
        NSString *txtForInit = [xAxisLabels objectAtIndex:labelLocation++];
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:txtForInit textStyle:xAxisTextStyle];
        newLabel.tickLocation = [tickLocation decimalValue];
        newLabel.offset       = x.labelOffset + x.majorTickLength;
        
        [customLabels addObject:newLabel];
        count++;
    }
    
    x.axisLabels = [NSSet setWithArray:customLabels];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    
    CPTXYAxis *y = axisSet.yAxis;
    y.axisLineStyle               = nil;
    y.majorTickLineStyle          = nil;
    y.minorTickLineStyle          = nil;
    
    y.majorIntervalLength         = CPTDecimalFromString(yMajorIntervalLength);
    y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0");
    y.title                       = @"Amount";
    
    if(isIPad)
        y.titleOffset                 = 80.0f;
    else
        y.titleOffset                 = 50.0f;
        
    y.labelTextStyle = yAxisTextStyle;
    
    if(isIPad)
        y.labelOffset = 30.0f;
    else
        y.labelOffset = 20.0f;
        
    y.majorGridLineStyle = lineStyleForGrid;
    y.labelFormatter = formatter;
    
    // First bar plot
    
    CPTBarPlot *barPlot = [[CPTBarPlot alloc] init];
    
    if(isIPad)
        [barPlot setCornerRadius:0.5f];
    else
        [barPlot setCornerRadius:0.2f];
        
    barPlot.fill = [CPTFill fillWithColor:[CPTColor orangeColor]];
    
    CPTMutableLineStyle *barOutlines = [[CPTMutableLineStyle alloc] init];
    
    if(isIPad)
        barOutlines.lineWidth = 2.0f;
    else
       barOutlines.lineWidth = 1.0f;
        
    barOutlines.lineColor = [CPTColor blackColor];
    
    
    
    if([salesData.calViewType isEqualToString:@"Day"])
        barPlot.barWidth = CPTDecimalFromString(@"0.8");
    if([salesData.calViewType isEqualToString:@"Month"])
        barPlot.barWidth = CPTDecimalFromString(@"0.5");
    if([salesData.calViewType isEqualToString:@"Year"])
        barPlot.barWidth = CPTDecimalFromString(@"0.5");
    
    barPlot.lineStyle = barOutlines;
    barPlot.baseValue  = CPTDecimalFromString(@"0");
    barPlot.dataSource = self;
    
    barPlot.identifier = @"Bar Plot 1";
    [barChart addPlot:barPlot toPlotSpace:plotSpace];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
