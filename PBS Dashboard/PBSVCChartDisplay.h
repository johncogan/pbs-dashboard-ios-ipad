//
//  PBSVCChartDisplay.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/10/02.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBSSales.h"
#import "PBSDataHourSalesItem.h"

#import "CorePlot-CocoaTouch.h"

@interface PBSVCChartDisplay : UIViewController <CPTPlotDataSource>
{
    @private
    CPTXYGraph *barChart;
}

@property (nonatomic, strong) PBSSales *salesData;

@property (nonatomic, strong) NSMutableArray *data;
@property (strong, nonatomic) IBOutlet CPTGraphHostingView *graphHostingView;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *detailsNavBarButton;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;

- (void) makeChart;

@end
