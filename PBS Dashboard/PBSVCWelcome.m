//
//  PBSWelcomeViewController.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSVCWelcome.h"
#import "PBSVCRequest.h"
#import "PBSDataRequest.h"
#import <QuartzCore/QuartzCore.h>
#import <Crashlytics/Crashlytics.h>

@interface PBSVCWelcome ()

@end

@implementation PBSVCWelcome

@synthesize targetDatePicker;
@synthesize segCtrlUserChosenDataView;
@synthesize dateDataScrollView;
@synthesize viewForDtPkr;
@synthesize salesData;

NSUserDefaults *userSettingsFromSettingsBundle;
NSString *settingsServer;
NSString *settingsPort;
NSString *settingsPassPhrase;
NSString *calType;
NSString *targetDate;
BOOL errorNoData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        errorNoData = NO;
    }
    return self;
}

-(void) setErrorNoData:(BOOL)hasError{
    self.errorNoData = hasError;
}
/* Comment for git prior to port to Universal app */
/*
    Check that there is a network connection
 */
-(NetworkStatus) checkNetworkStatus
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

/*
 Validate that the user has provided the server url, port number and pass phrase
 for the point of sales server and return NO if they are not complete.
*/
-(BOOL) validateServerSettings
{
    NSString *serverUrl;
    NSString *portNumber;
    NSString *passPhrase;
    
    BOOL settingsAreValid = NO;
    
    userSettingsFromSettingsBundle = [NSUserDefaults standardUserDefaults];
    
    serverUrl = [userSettingsFromSettingsBundle objectForKey:@"server_url_preference"];
    portNumber = [userSettingsFromSettingsBundle objectForKey:@"server_port_preference"];
    passPhrase = [userSettingsFromSettingsBundle objectForKey:@"passphrase_preference"];
    
    if([serverUrl length] > 0 && [portNumber length] > 0 && [passPhrase length] > 0){
        settingsAreValid = YES;
    }
    
    return settingsAreValid;
}

-(void)customiseControls{
    //UIColor *pbsOrange = [UIColor colorWithRed:252.0/255 green:158.0/255 blue:9.0/255 alpha:1.0f].CGColor;
    UIColor *ios7DefaultBlue = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255/255 alpha:1.0f].CGColor;

    
    [[_btnRequest layer] setBorderWidth:1.0f];
    _btnRequest.layer.cornerRadius = 5.0f;
   
    _btnRequest.clipsToBounds = YES;
    [[_btnRequest layer] setBorderColor:(__bridge CGColorRef)(ios7DefaultBlue)];
    [[_btnRequest layer] setBackgroundColor:(__bridge CGColorRef)(ios7DefaultBlue)];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [self customiseControls];
    
    if(self.salesData != nil || self.salesData != NULL){
        if([salesData.error isEqual:@"NODATA"]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No data returned" message:@"No data was returned for the selected date, please choose a different date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
        
        if([salesData.error isEqual:@"InvalidPassphrase"]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect password" message:@"You did not provide the correct password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
        }
    }
}

/*
 Do network checks and application settings prior to performing the swgue and display
 error notices if anything is amiss and cancel the segue
 */
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    BOOL appSettingsAreConfigured = NO;
    
    // Has the user set up the application settings?
    if([self validateServerSettings]){
        appSettingsAreConfigured = YES;
    }
    
    // Perform this if the identifier is: RequestData
    if([identifier isEqualToString:@"RequestData"]){
        // Crashlytics testing
        //[[Crashlytics sharedInstance] crash];
        
        // If app settings are setup and correct
        if(appSettingsAreConfigured)
        {
            // Check there is a network connection
            if([self checkNetworkStatus]){
                return YES;
            }else{
                // No network connection. Stop the segue and display error message.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No network connection" message:@"You must be connected to the internet to use this app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
                
                return NO;
            }
        }else{
            // App settings are not valid, stop segue and disp[lay error message
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Application settings are not correct." message:@"You must provide the url, port number and pass phrase to your Point of Sales server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
            [alert show];
        
            return NO;
        }
    }else{
        // Not the correct identifier
        return NO;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"RequestData"]){
        PBSVCRequest *requestViewController = [segue destinationViewController];
        PBSDataRequest *requestParameters = [[PBSDataRequest alloc] init];
        
        [requestParameters setCalendType:calType];
        [requestParameters setTargetDate:targetDate];
        
        [requestViewController setRequestParameters:requestParameters];
    }
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self resizeScrollView];
}

/*
 Resize the scroll view according to the size of the client display bounds
 */
-(void)resizeScrollView
{
    [dateDataScrollView setScrollEnabled:YES];
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self loadDefaultSettings];
    
    [targetDatePicker setDate:[NSDate date]];
    [targetDatePicker setMaximumDate:[NSDate date]];
    [segCtrlUserChosenDataView setSelectedSegmentIndex:0];
    
    // Default the calType variable if it is nil
    if(calType == nil){
        calType = @"Day";
    }
    
    // Default the target date to todays date if targetDate is nil
    if(targetDate == nil){
        NSDate *dateFromDatePicker = targetDatePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *formattedDateFromPicker = [dateFormatter stringFromDate:dateFromDatePicker];
        
        targetDate = formattedDateFromPicker;
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindToTopLevel:(UIStoryboardSegue *)segue
{
    
}

- (IBAction)calendarType:(id)sender
{
    calType = [segCtrlUserChosenDataView titleForSegmentAtIndex:segCtrlUserChosenDataView.selectedSegmentIndex];
}

- (IBAction)targetDatePicked:(id)sender
{
    NSDate *dateFromDatePicker = targetDatePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDateFromPicker = [dateFormatter stringFromDate:dateFromDatePicker];
    
    targetDate = formattedDateFromPicker;
}

- (void) loadDefaultSettings
{
    userSettingsFromSettingsBundle = [NSUserDefaults standardUserDefaults];
    
    settingsServer = [userSettingsFromSettingsBundle objectForKey:@"server_url_preference"];
    settingsPort = [userSettingsFromSettingsBundle objectForKey:@"server_port_preference"];
    settingsPassPhrase = [userSettingsFromSettingsBundle objectForKey:@"passphrase_preference"];
}
@end
