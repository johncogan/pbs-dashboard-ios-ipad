//
//  PBSHourSalesItem.m
//  PBSDashboard
//
//  Created by John Cogan on 2013/09/11.
//  Copyright (c) 2013 Platinum Point of Sale Business Solutions. All rights reserved.
//

#import "PBSDataHourSalesItem.h"
#import <Crashlytics/Crashlytics.h>

@implementation PBSDataHourSalesItem
{

}

@synthesize hour;
@synthesize numberOfSales;
@synthesize totalOfSales;

- (id)init
{
    if(self = [super init])
    {
        hour = 0;
        numberOfSales = 0;
        totalOfSales = 0;
    }
    return self;
}

- (id)initWithHour:(NSUInteger)theHour
{
    self.hour = theHour;
    self.numberOfSales = 0;
    self.totalOfSales = 0;
    
    return self;
}
@end
