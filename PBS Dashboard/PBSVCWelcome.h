//
//  PBSWelcomeViewController.h
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBSSales.h"
#import "PBSDataRequest.h"
#import "Reachability.h"

@class Reachability;

@interface PBSVCWelcome : UIViewController
{
    Reachability* internetReachable;
    Reachability* hostReachable;
}
@property (weak, nonatomic) IBOutlet UIDatePicker *targetDatePicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segCtrlUserChosenDataView;
@property (weak, nonatomic) IBOutlet UIView *viewForDtPkr;
@property (strong, nonatomic) PBSSales *salesData;

- (IBAction)targetDatePicked:(id)sender;
- (IBAction)calendarType:(id)sender;

- (void) loadDefaultSettings;
@property (weak, nonatomic) IBOutlet UIScrollView *dateDataScrollView;

- (IBAction)unwindToTopLevel:(UIStoryboardSegue *)segue;

-(NetworkStatus) checkNetworkStatus;
-(BOOL) validateServerSettings;
-(void) setErrorNoData:(BOOL)hasError;
-(void)customiseControls;

@property (weak, nonatomic) IBOutlet UIButton *btnRequest;

@end
