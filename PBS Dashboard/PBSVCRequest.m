//
//  PBSRequestViewController.m
//  PBS Dashboard
//
//  Created by John Cogan on 2013/09/23.
//  Copyright (c) 2013 Platinum POS. All rights reserved.
//

#import "PBSVCRequest.h"
#import "PBSVCWelcome.h"
#import "PBSVCDataDisplay.h"
#import "SBJson.h"
#import "UNIRest.h"

#import <Crashlytics/Crashlytics.h>

@interface PBSVCRequest ()

@end

@implementation PBSVCRequest


@synthesize requestVCMessages;
@synthesize userChosenDataDisplayType;
@synthesize userChosenTargetDate;
@synthesize requestParameters;

#pragma mark ViewController scoped variables

NSUserDefaults *defaults;
NSString *settingsServer;
NSString *settingsPort;
NSString *settingsPassPhrase;

NSString *requestUrl;

dispatch_queue_t unirestQueue;
//HttpJsonResponse *responseVal;

UIActivityIndicatorView *activityIndicator;
PBSDataRequest *requestInfo;

# pragma mark Boiler plate code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        requestUrl = [[NSString alloc] init];
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_activity setHidden:NO];
}

-(void)viewDidAppear{
    
}

- (IBAction)unWindToHome:(id)sender {
    [self performSegueWithIdentifier:@"unwindToTopLevel" sender:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [[self requestVCMessages] setTextAlignment:NSTextAlignmentCenter];
    [[self requestVCMessages] setText:@"Requesting data from the server"];
    
    [self loadDefaultSettings];
    
    
    
    [requestVCMessages setText:@""];
    
    requestUrl = [self buildRequestUrl:settingsServer withPort:settingsPort withCalenderType:[requestParameters calendType] withDateStringParameter:[requestParameters targetDate]];
    
    // Do any additional setup after loading the view from its nib.
    salesData = [[PBSSales alloc] init];
    
    //  Start block on background queue so the main thread is not frozen
    //  which prevents apps UI freeze
    [self runUnirestRequest:requestUrl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark User created methods

/*
    Load the settings stored within the settings bundle
 */
- (void) loadDefaultSettings
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    settingsServer = [defaults objectForKey:@"server_url_preference"];
    settingsPort = [defaults objectForKey:@"server_port_preference"];
    settingsPassPhrase = [defaults objectForKey:@"passphrase_preference"];
}



/*
    Build and return the URL that will be queried for data
 */
- (NSString*) buildRequestUrl:(NSString*)serverUrl withPort:(NSString*)serverPort withCalenderType:(NSString*)dataDisplayType withDateStringParameter:(NSString*)selectedTargetDate
{
    NSString *url = [serverUrl stringByAppendingString:@":"];
    
    url = [url stringByAppendingString:serverPort];
    
    // Default to Day Sales data if there was no calendar chosen
    if([dataDisplayType isEqual:nil])
    {
        url = [url stringByAppendingString:@"/Sales/Day/"];
    }else{
        if([dataDisplayType isEqual: @"Day"]){
            url = [url stringByAppendingString:@"/Sales/Day/"];
        }
        
        if([dataDisplayType isEqual: @"Month"]){
            url = [url stringByAppendingString:@"/Sales/Month/"];
        }
        
        if([dataDisplayType isEqual: @"Year"]){
            url = [url stringByAppendingString:@"/Sales/Year/"];
        }
    }
    
    if(![selectedTargetDate isEqual:nil])
    {
        url = [url stringByAppendingString:selectedTargetDate];
    }else{
        
    }
    
    return url;
}

/*
    Initialize an Activity Indicator
 */
-(void) createActivityIndicator
{
    _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activity.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    _activity.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    float xValue = _subView.bounds.size.width/2.0f;
    float yValue = _subView.bounds.size.height/2.0f;
    
    CGPoint activityCenter = CGPointMake(xValue, yValue);
    
    _activity.center = activityCenter;
    
    [self.view addSubview:_activity];
}

/*
    Transform the Json values into the object for passing around
    TODO: Move into PBSDaySales class
 */
- (PBSSales*) deserializeJsonPacket:(NSDictionary*)jsonResponseAsDictionary withCalenderType:(NSString *)calendarViewType
{
    PBSSales *pbsData = [[PBSSales alloc] init];
    
    if(jsonResponseAsDictionary != nil)
    {
        [pbsData setError:[jsonResponseAsDictionary objectForKey:@"error"]];
        if(pbsData.error != NULL){
            [pbsData setTargetDate:[jsonResponseAsDictionary objectForKey:@"targetDate"]];
            [pbsData setPreviousDate:[jsonResponseAsDictionary objectForKey:@"previousDate"]];
            [pbsData setNextDate:[jsonResponseAsDictionary objectForKey:@"nextDate"]];
        
            [pbsData setNumberOfCashSales:[jsonResponseAsDictionary objectForKey:@"numberOfCashSales"]];
            [pbsData setNumberOfCardSales:[jsonResponseAsDictionary objectForKey:@"numberOfCardSales"]];
            [pbsData setNumberOfChequeSales:[jsonResponseAsDictionary objectForKey:@"numberOfChequeSales"]];
            [pbsData setNumberOfLoyaltySales:[jsonResponseAsDictionary objectForKey:@"numberOfLoyaltySales"]];
            [pbsData setNumberOfChargeSales:[jsonResponseAsDictionary objectForKey:@"numberOfChargeSales"]];
        
            [pbsData setTotalCash:[jsonResponseAsDictionary objectForKey:@"totalCash"]];
            [pbsData setTotalCard:[jsonResponseAsDictionary objectForKey:@"totalCard"]];
            [pbsData setTotalCheque:[jsonResponseAsDictionary objectForKey:@"totalCheque"]];
            [pbsData setTotalLoyalty:[jsonResponseAsDictionary objectForKey:@"totalLoyalty"]];
            [pbsData setTotalCharge:[jsonResponseAsDictionary objectForKey:@"totalCharge"]];
        
            [pbsData setTotalGrand:[jsonResponseAsDictionary objectForKey:@"totalGrand"]];
            
            [salesData reset];
            
            // Process the hourly sales figures if the day request and returned is related to Daily figures
            if([calendarViewType isEqualToString:@"Day"]){
                NSArray *hourlyFiguresFromJson = [jsonResponseAsDictionary objectForKey:@"hourlySalesFigures"];
                
                PBSDataDaySales *tmpDataDay = [[PBSDataDaySales alloc] init];
                NSMutableArray *hSalesFigures = [tmpDataDay hourlySalesFigures];
                
                for(NSInteger i = 0; i < [hourlyFiguresFromJson count]; i++){
                    hSalesFigures[i] = hourlyFiguresFromJson[i];
                }
                
                [[pbsData dataDay] setHourlySalesFigures:hSalesFigures];
                [pbsData setCalViewType:@"Day"];
            }
            
            // Process the daily sales figures if the day request and returned is related to Monthly figures
            if([calendarViewType isEqualToString:@"Month"]){
                NSString *numberOfDaysInMonth = [jsonResponseAsDictionary objectForKey:@"numDaysInMonth"];
                NSArray *monthlyFiguresFromJson = [jsonResponseAsDictionary objectForKey:@"monthlySalesFigures"];
                
                PBSDataMonthSales *tmpDataMonth = [[PBSDataMonthSales alloc] initWithSetNumberOfDays:[numberOfDaysInMonth integerValue]];
                NSMutableArray *mSalesFigures = [tmpDataMonth dailySalesFigures];
                
                for(NSInteger i = 0; i < [monthlyFiguresFromJson count]; i++){
                    mSalesFigures[i] = monthlyFiguresFromJson[i];
                }
                
                [[pbsData dataMonth] setDailySalesFigures:mSalesFigures];
                [pbsData setCalViewType:@"Month"];
            }
            
            // Process the monthly sales figures if the day request and returned is related to Yearly figures
            if([calendarViewType isEqualToString:@"Year"]){
                NSArray *monthlyFiguresFromJson = [jsonResponseAsDictionary objectForKey:@"yearlySalesFigures"];
                
                PBSDataYearSales *tmpDataDay = [[PBSDataYearSales alloc] init];
                NSMutableArray *mSalesFigures = [tmpDataDay monthlySalesFigures];
                
                for(NSInteger i = 0; i < [monthlyFiguresFromJson count]; i++){
                    mSalesFigures[i] = monthlyFiguresFromJson[i];
                }
                
                [[pbsData dataYear] setMonthlySalesFigures:mSalesFigures];
                [pbsData setCalViewType:@"Year"];
            }
        }
        
    }else{
        [pbsData setError:@"PBSRequestViewController->deserializeJsonPacket-> No data in response (UniRest->deserializeJsonPacket)"];
        
        // We want to display the error returned from the server response.
        [requestVCMessages setText:[@"A server error has occured\n\n" stringByAppendingString:[jsonResponseAsDictionary objectForKey:@"error"]]];
    }
    
    return pbsData;
}

/**
    Prior to executing a segue, here we get any arguments or objects ready for passing between view controllers, if applicable.
 **/


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //NSLog(@"PBSVCRequest - prepareForSegue");
    
    if([segue.identifier isEqualToString:@"showDataChart"]){
        //NSLog(@"segue = showDataChart");
        // If the segue been called is "showDataChart", pass it the daySalesFigures data object
        PBSVCDataDisplay *dataVc = [segue destinationViewController];
        
        [dataVc setSalesData:salesData];
    }
}

/*
    Method will forward the user on to the data view.
    Used after the uniRestRequest is completed and data exists.
*/
- (void) sequeToDataDisplayViewController
{
    //NSLog(@"PBSVCRequest - sequeToDataDisplayViewController");
    // Only to seque to data view if data exists, else display error message
    if(salesData != nil){
        
    }else{
        if([salesData.error isEqualToString:@"InvalidPassphrase"]){
            //[requestVCMessages setText:@"Incorrect passphrase provided."];
        }else{
            //[requestVCMessages setText:@"No data was returned for the selected date."];
        }
    }
}

- (void) runUnirestRequest:(NSString*)urlToSendRequestTo
{
    //NSLog(@"runUnirestRequest - Started");
    [self loadDefaultSettings];
    [self createActivityIndicator];
    
    [requestVCMessages setTextAlignment:NSTextAlignmentCenter];
    [requestVCMessages setText:@"Processing request"];
    
        NSDictionary* headers = @{@"p": settingsPassPhrase};
    
    [_activity startAnimating];
    
        [[UNIRest get:^(UNISimpleRequest* request) {
            [request setUrl:urlToSendRequestTo];
            [request setHeaders:headers];
        }] asJsonAsync:^(UNIHTTPJsonResponse* response, NSError *error) {
            if(error)
            {
                //NSLog(@"Error in async");
            }
            
            UNIJsonNode *jsonNde = [response body];
            
            NSDictionary *jsonAsDictionary = jsonNde.JSONObject;
            
            salesData = [self deserializeJsonPacket:(NSDictionary*)jsonAsDictionary withCalenderType:[requestParameters calendType]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_activity stopAnimating];
                
                PBSVCDataDisplay *dataVc = [[PBSVCDataDisplay alloc] init];
                
                if([salesData.error isEqual:[NSNull null]]){
                    if([salesData.totalGrand floatValue] == 0.0f){
                        salesData.error = @"NODATA";
                    }else{
                        [dataVc setSalesData:salesData];
                        
                        [self performSelector:@selector(moveToDChart) withObject:nil afterDelay:1.0];
                    }
                }else{
                    if([salesData.error isEqualToString:@"InvalidPassphrase"]){
                        [requestVCMessages setText:@"Incorrect passphrase provided."];
                    }else{
                        salesData.error = @"NODATA";
                        //[requestVCMessages setText:@"No data was returned for the selected date."];
                    }
                }
                
                
                
                //NSLog(@"runUnirestRequest - Finished");
                
            });
        }];
}

- (void) moveToDChart
{
        [self performSegueWithIdentifier:@"showDataChart" sender:self];
}

@end
